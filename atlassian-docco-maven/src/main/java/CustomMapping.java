/**
 * @since 1.0
 */
public class CustomMapping
{
    private String extension;
    private String commentType;
    private String syntax;
    private Boolean stripJavadoc;

    public String getExtension()
    {
        return extension;
    }

    public void setExtension(String extension)
    {
        this.extension = extension;
    }

    public String getCommentType()
    {
        return commentType;
    }

    public void setCommentType(String commentType)
    {
        this.commentType = commentType;
    }

    public String getSyntax()
    {
        return syntax;
    }

    public void setSyntax(String syntax)
    {
        this.syntax = syntax;
    }

    public Boolean getStripJavadoc()
    {
        return stripJavadoc;
    }

    public void setStripJavadoc(Boolean stripJavadoc)
    {
        this.stripJavadoc = stripJavadoc;
    }
}
