import java.io.File;
import java.util.Arrays;

import com.atlassian.docco.DoccoBatch;
import com.atlassian.docco.builder.DoccoBatchBuilder;
import com.atlassian.docco.mapping.DoccoCommentPatterns;
import com.atlassian.docco.mapping.DoccoCommentPatternsBuilder;
import com.atlassian.docco.mapping.DoccoFileMapping;
import com.atlassian.docco.mapping.DoccoFileMappingManager;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.jfrog.maven.annomojo.annotations.MojoGoal;
import org.jfrog.maven.annomojo.annotations.MojoParameter;

import static com.google.common.base.Preconditions.checkState;

@MojoGoal("docco")
public class DoccoMojo extends AbstractMojo
{
    @MojoParameter(expression = "${docco.source.dir}", required = true, defaultValue = "${basedir}")
    protected File sourceDirectory;

    @MojoParameter(expression = "${docco.output.dir}", required = true, defaultValue = "${project.build.directory}/docco")
    protected File outputDirectory;

    @MojoParameter(expression = "${excludes}")
    protected String[] excludes;

    @MojoParameter(expression = "${includes}")
    protected String[] includes;

    @MojoParameter(expression = "${docco.horizontal.template}")
    protected File horizontalTemplate;

    @MojoParameter(expression = "${docco.vertical.template}")
    protected File verticalTemplate;

    @MojoParameter(expression = "${docco.custom.mappings}")
    private CustomMapping[] customMappings;

    @MojoParameter(expression = "${docco.index.title}", required = true, defaultValue = "Docco Index")
    protected String indexTitle;

    @MojoParameter(expression = "${docco.index.filename}", required = true, defaultValue = "index.html")
    protected String indexFilename;

    @MojoParameter(expression = "${docco.custom.resources}", required = true, defaultValue = "${build.outputDirectory}")
    protected File[] customResources;

    @MojoParameter(expression = "${docco.strip.javadoc}", required = true, defaultValue = "true")
    protected Boolean stripJavadoc;

    @MojoParameter(expression = "${docco.skip.no.docco}", required = true, defaultValue = "true")
    protected Boolean skipNoDocco;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        //TODO: exclude hidden and target directories
        DoccoBatchBuilder builder = DoccoBatch.builder(sourceDirectory,outputDirectory);

        if(null != horizontalTemplate)
        {
            checkState(horizontalTemplate.exists());
            builder.horizontalTemplate(horizontalTemplate);
        }

        if(null != verticalTemplate)
        {
            checkState(verticalTemplate.exists());
            builder.verticalTemplate(verticalTemplate);
        }

        if(null != excludes)
        {
            builder.excludes(excludes);
        }

        if(null != includes)
        {
            builder.includes(includes);
        }

        builder.indexTitle(indexTitle);
        builder.indexFilename(indexFilename);
        builder.stripJavadoc(stripJavadoc);
        builder.skipNoDocco(skipNoDocco);

        if(null != customMappings && customMappings.length > 0)
        {
            DoccoFileMappingManager mappingManager = new DoccoFileMappingManager();
            for(Object mappingObj : customMappings)
            {
                CustomMapping mapping = (CustomMapping) mappingObj;
                checkState(StringUtils.isNotBlank(mapping.getExtension()));
                checkState(StringUtils.isNotBlank(mapping.getCommentType()));
                checkState(StringUtils.isNotBlank(mapping.getSyntax()));

                String type = mapping.getCommentType();
                checkState("code".equalsIgnoreCase(type) || "markup".equalsIgnoreCase(type) || "text".equalsIgnoreCase(type));

                DoccoCommentPatterns patterns = null;

                if("code".equalsIgnoreCase(type))
                {
                    patterns = DoccoCommentPatternsBuilder.CODE_PATTERNS;
                }
                else if("markup".equalsIgnoreCase(type))
                {
                    patterns = DoccoCommentPatternsBuilder.MARKUP_PATTERNS;
                }
                else if("text".equalsIgnoreCase(type))
                {
                    patterns = DoccoCommentPatternsBuilder.TEXT_PATTERNS;
                }

                DoccoFileMapping doccoMapping = new DoccoFileMapping(patterns,mapping.getSyntax(),!mapping.getStripJavadoc());
                mappingManager.addMappingForFileExtension(mapping.getExtension(),doccoMapping);
            }

            builder.fileMappings(mappingManager);
        }

        if(null != customResources && customResources.length > 0)
        {
            builder.resources(Arrays.asList(customResources));
        }

        DoccoBatch batch = builder.build();

        try
        {
            batch.generateDocco();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new MojoExecutionException("Unable to generate Docco!", e);
        }

    }
}
