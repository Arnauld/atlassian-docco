package com.atlassian.docco;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.parboiled.common.FileUtils;

public class SmokeTest
{
    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void simpleJava() throws IOException, URISyntaxException
    {
        URL url = this.getClass().getResource("/JavaFile.java");
        File me = new File(url.getFile());

        File htmlFile = new File(tempFolder.getRoot(), "PegdownTest.html");

        Docco docco = Docco.builder().build();

        docco.writeHtml(me,htmlFile);

    }

    @Test
    public void simpleHtml() throws IOException, URISyntaxException
    {
        URL url = this.getClass().getResource("/simple.html");
        File me = new File(url.getFile());

        File htmlFile = new File(tempFolder.getRoot(), "HTMLTest.html");

        Docco docco = Docco.builder().build();

        docco.writeHtml(me,htmlFile);

    }

    @Test
    public void tryABatch() throws Exception
    {
        URL url = this.getClass().getResource("/JavaFile.java");
        File me = new File(url.getFile());
        File base = me.getParentFile();

        DoccoBatch batch = DoccoBatch.builder(base,tempFolder.getRoot()).build();

        batch.generateDocco();
    }
}
