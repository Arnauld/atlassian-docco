package com.atlassian.docco;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.pegdown.PegDownProcessor;

/**
 * @since 1.0
 */
public class Header
{
    private String doc;
    private String markdown;
    private PegDownProcessor pegDown;

    public Header()
    {
        this(new PegDownProcessor());
    }

    public Header(PegDownProcessor pegDown)
    {
        this.doc = "";
        this.markdown = null;
        this.pegDown = pegDown;
    }

    public Header addDoc(String doc)
    {
        this.doc += doc + "\n";
        this.markdown = null;

        return this;
    }

    public boolean isEmpty()
    {
        return StringUtils.isBlank(doc);
    }

    public String getDoc()
    {
        if(null == markdown)
        {
            markdown = pegDown.markdownToHtml(doc);
        }

        return markdown;
    }

    public String getDocWithTokenReplacement(Map<String,String> replaceMap)
    {
        String replacedDoc = doc;
        for(Map.Entry<String,String> entry : replaceMap.entrySet())
        {
            replacedDoc = StringUtils.replace(doc,entry.getKey(),entry.getValue());
        }

        if(null == markdown)
        {
            markdown = pegDown.markdownToHtml(replacedDoc);
        }

        return markdown;
    }
}
