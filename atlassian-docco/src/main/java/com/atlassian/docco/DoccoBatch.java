package com.atlassian.docco;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

import javax.annotation.Nullable;

import com.atlassian.docco.builder.DoccoBatchBuilder;
import com.atlassian.docco.mapping.DoccoFileMappingManager;

import com.google.common.base.Function;
import com.google.common.collect.*;
import com.google.template.soy.SoyFileSet;
import com.google.template.soy.data.SanitizedContent;
import com.google.template.soy.data.SoyListData;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.tofu.SoyTofu;

import org.apache.commons.io.DirectoryWalker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.plexus.util.DirectoryScanner;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * @since 1.0
 */
public final class DoccoBatch
{
    public static final String[] DEFAULT_EXCLUDES = new String[]{"**/.*/","**/.*","**/target/**"};
    private final File basePath;
    private final File outputPath;
    private final DoccoFileMappingManager fileMappings;
    private final String title;
    private final String indexFilename;
    private final String[] excludes;
    private final String[] includes;
    private final boolean skipNoDocco;
    private final Docco docco;
    private final SoyTofu horizontalTofu;
    private final SoyTofu verticalTofu;


    public DoccoBatch(Docco docco, File basePath, File outputPath, String[] excludes, String[] includes, URL horizonatlSoyTemplate, URL verticalSoyTemplate, DoccoFileMappingManager fileMappings, String title, String indexFilename, boolean skipNoDocco)
    {
        this.docco = docco;
        this.basePath = basePath;
        this.outputPath = outputPath;
        this.excludes = excludes;
        this.includes = includes;
        this.fileMappings = fileMappings;
        this.title = title;
        this.indexFilename = indexFilename;
        this.skipNoDocco = skipNoDocco;

        SoyFileSet soyFileSetH = new SoyFileSet.Builder().add(horizonatlSoyTemplate).build();
        this.horizontalTofu = soyFileSetH.compileToJavaObj().forNamespace("atlassian.docco");

        SoyFileSet soyFileSetV = new SoyFileSet.Builder().add(verticalSoyTemplate).build();
        this.verticalTofu = soyFileSetV.compileToJavaObj().forNamespace("atlassian.docco");
    }

    public void generateDocco() throws URISyntaxException, IOException
    {
        File hBase = new File(outputPath, "horizontal");
        FileUtils.forceMkdir(hBase);

        File vBase = new File(outputPath, "vertical");
        FileUtils.forceMkdir(vBase);

        docco.copyResources(hBase,vBase);

        String[] extensions = Lists.transform(fileMappings.getMappedFileExtensions(), new Function<String, String>()
        {
            @Override
            public String apply(@Nullable String s)
            {
                return "**/*." + s;
            }
        }).toArray(new String[0]);


        DirectoryScanner scanner = new DirectoryScanner();
        scanner.setBasedir(basePath);
        scanner.setIncludes(getIncludes(extensions));
        scanner.setExcludes(getExcludes());
        scanner.addDefaultExcludes();
        scanner.scan();

        Collection<File> sourceFiles = Collections2.transform(Arrays.asList(scanner.getIncludedFiles()), new Function<String, File>() {
            @Override
            public File apply(@Nullable String s)
            {
                return new File(basePath,s);
            }
        });

        List<DoccoResult> results = new ArrayList<DoccoResult>(sourceFiles.size());
        for (File sourceFile : sourceFiles)
        {
            DoccoResult result = doccoSourceFile(sourceFile);

            if(null != result)
            {
                results.add(result);
            }
        }

        Multimap<String,String> hIndexMap = createIndexMap(results,hBase);
        Multimap<String,String> vIndexMap = createIndexMap(results,vBase);

        SoyListData hSoyIndex = getSoyIndexFiles(hIndexMap);
        SoyListData vSoyIndex = getSoyIndexFiles(vIndexMap);

        writeHtmlFiles(results,hBase,vBase,hSoyIndex,vSoyIndex);


        doccoIndex(hSoyIndex,hBase,horizontalTofu);
        doccoIndex(vSoyIndex,vBase,verticalTofu);

    }

    private DoccoResult doccoSourceFile(File sourceFile) throws IOException
    {
        DoccoParts parts = docco.getDoccoParts(sourceFile);

        List<Section> sections = parts.getSections();

        if(skipNoDocco && (sections.size() < 2 && parts.getHeader().isEmpty()))
        {
            return null;
        }

        return new DoccoResult(sourceFile,parts);
    }

    private void writeHtmlFiles(List<DoccoResult> results,File hBase, File vBase, SoyListData hIndex, SoyListData vIndex) throws IOException
    {
        for(DoccoResult result : results)
        {
            String syntax = fileMappings.getSyntaxForFile(result.getSourceFile());
            writeHtml(result.getSourceFile(), syntax, result.getParts().getHeader(), result.getParts().getSections(), hIndex, hBase, horizontalTofu);
            writeHtml(result.getSourceFile(), syntax, result.getParts().getHeader(), result.getParts().getSections(), vIndex, vBase, verticalTofu);
        }
    }

    private File writeHtml(File sourceFile, String syntax, Header header, List<Section> sections, SoyListData index, File baseOutputPath, SoyTofu tofu) throws IOException
    {
        String relativeName = getRelativeName(sourceFile);
        File htmlFile = new File(baseOutputPath, relativeName);

        FileUtils.forceMkdir(htmlFile.getParentFile());
        String relativeBase = getRelativeBase(htmlFile,baseOutputPath);

        Map<String,String> replaceMap = ImmutableMap.<String,String> builder().put("${basePath}",relativeBase).build();

        SanitizedContent headerHtml = new SanitizedContent(header.getDocWithTokenReplacement(replaceMap), SanitizedContent.ContentKind.HTML);
        SoyMapData soyData = new SoyMapData("title", StringUtils.substringBeforeLast(htmlFile.getName(), "."),"nameWithoutExtension", StringUtils.substringBeforeLast(sourceFile.getName(), "."),"syntax", syntax, "groups", index, "basePath", relativeBase, "myPath", relativeName, "header", headerHtml, "sections", Docco.getSoySectionDataWithTokenReplacement(sections, replaceMap));
        String rendered = tofu.newRenderer(".batchPage").setData(soyData).render();
        FileUtils.writeStringToFile(htmlFile, rendered);

        return htmlFile;
    }

    private File doccoIndex(SoyListData index, File baseOutputPath, SoyTofu tofu) throws IOException
    {
        SoyMapData soyData = new SoyMapData("basePath",".","title",title,"groups",index);
        String rendered = tofu.newRenderer(".indexPage").setData(soyData).render();

        File indexFile = new File(baseOutputPath,indexFilename);
        FileUtils.touch(indexFile);

        FileUtils.writeStringToFile(indexFile, rendered);

        return  indexFile;
    }

    private SoyListData getSoyIndexFiles(Multimap<String, String> indexMap) throws IOException
    {

        SoyListData listData = new SoyListData();

        for(String group : indexMap.keySet())
        {

            SoyListData fileList = new SoyListData();

            Collection<String> htmlFiles = indexMap.get(group);

            for(String htmlFile : htmlFiles)
            {
                SoyMapData fileMap = new SoyMapData("dir",StringUtils.substringBeforeLast(htmlFile,"/"),"file",StringUtils.substringAfterLast(htmlFile,"/"));
                fileList.add(fileMap);
            }

            SoyMapData dirMap = new SoyMapData("name",group,"files",fileList);
            listData.add(dirMap);
        }

        return listData;
    }

    private Multimap<String, String> createIndexMap(List<DoccoResult> results, File baseOutputPath) throws IOException
    {
        Multimap<String, String> indexMap = ArrayListMultimap.<String,String>create();

        for(DoccoResult result : results)
        {
            String relativeName = getRelativeName(result.getSourceFile());

            for(String group : result.getParts().getGroups())
            {
                String groupName = group;
                if(group.startsWith("file://"))
                {
                    String groupPath = StringUtils.substringAfter(group,"file://");
                    if(groupPath.equals(basePath.getCanonicalPath()))
                    {
                        groupName = groupPath + "/";
                    }
                    else
                    {
                        groupName = groupPath.substring(basePath.getCanonicalPath().length() + 1) + "/";
                    }
                }

                String groupKey = StringUtils.replace(groupName,File.separator,"/");
                String fileValue = StringUtils.replace(StringUtils.substringBeforeLast(relativeName,"."),File.separator,"/");
                indexMap.put(groupName,fileValue);
            }
        }

        Multimap<String,String> sorted = ImmutableMultimap.<String,String> builder().orderKeysBy(new Comparator<String>() {
            @Override
            public int compare(String s1, String s2)
            {
                return s1.compareTo(s2);
            }
        }).putAll(indexMap).build();

        return sorted;
    }


    private String getRelativeBase(File htmlFile, File baseOutputPath) throws IOException
    {
        String relativeFile = StringUtils.substringAfter(htmlFile.getCanonicalPath(),baseOutputPath.getCanonicalPath());
        int numDirs = StringUtils.countMatches(relativeFile,File.separator) - 1;

        return StringUtils.chomp(StringUtils.repeat(".." + "/", numDirs), "/");
    }

    private String getRelativeName(File sourceFile) throws IOException
    {
        return sourceFile.getCanonicalPath().substring(basePath.getCanonicalPath().length() + 1) + ".html";
    }

    private String[] getExcludes()
    {
        int excludesLength = excludes == null ? 0 : excludes.length;
        String[] newExcludes;
        newExcludes = new String[excludesLength + DEFAULT_EXCLUDES.length];

        if(excludesLength > 0)
        {
            System.arraycopy( excludes, 0, newExcludes, 0, excludes.length );
        }

        for(int i=0; i<DEFAULT_EXCLUDES.length; i++)
        {
            newExcludes[i + excludesLength] = DEFAULT_EXCLUDES[i];
        }

        return newExcludes;
    }

    private String[] getIncludes(String[] defaultIncludes)
    {
        int includesLength = includes == null ? 0 : includes.length;
        String[] newIncludes;
        newIncludes = new String[includesLength + defaultIncludes.length];

        if(includesLength > 0)
        {
            System.arraycopy( includes, 0, newIncludes, 0, includes.length );
        }

        for(int i=0; i<defaultIncludes.length; i++)
        {
            newIncludes[i + includesLength] = defaultIncludes[i];
        }

        return newIncludes;
    }

    public static DoccoBatchBuilder builder(File basePath, File ouputPath)
    {
        return new DoccoBatchBuilder(basePath,ouputPath);
    }

    private class DoccoResult
    {
        private File sourceFile;
        private DoccoParts parts;

        private DoccoResult(File sourceFile, DoccoParts parts)
        {
            this.sourceFile = sourceFile;
            this.parts = parts;
        }

        public File getSourceFile()
        {
            return sourceFile;
        }

        public DoccoParts getParts()
        {
            return parts;
        }
    }

}
