package com.atlassian.docco.mapping;

import java.util.regex.Pattern;

/**
 * @since 1.0
 */
public class DoccoCommentPatternsBuilder
{
    public static final DoccoCommentPatterns CODE_PATTERNS = new DoccoCommentPatternsBuilder()
            .multilineBegin(DoccoCommentPatterns.CODE_DOCCO_MULTI_BEGIN)
            .multilineEnd(DoccoCommentPatterns.CODE_DOCCO_MULTI_END)
            .singleLine(DoccoCommentPatterns.CODE_DOCCO_SINGLE_LINE)
            .javadoc(DoccoCommentPatterns.CODE_JAVADOC_BEGIN)
            .groups(DoccoCommentPatterns.CODE_DOCCO_GROUPS)
            .build();

    public static final DoccoCommentPatterns MARKUP_PATTERNS = new DoccoCommentPatternsBuilder()
            .singleLine(DoccoCommentPatterns.MARKUP_DOCCO_SINGLE_LINE)
            .multilineBegin(DoccoCommentPatterns.MARKUP_DOCCO_MULTI_BEGIN)
            .multilineEnd(DoccoCommentPatterns.MARKUP_DOCCO_MULTI_END)
            .groups(DoccoCommentPatterns.MARKUP_DOCCO_GROUPS)
            .build();

    public static final DoccoCommentPatterns TEXT_PATTERNS = new DoccoCommentPatternsBuilder()
            .singleLine(DoccoCommentPatterns.TEXT_DOCCO_SINGLE_LINE)
            .groups(DoccoCommentPatterns.TEXT_DOCCO_GROUPS)
            .build();

    private Pattern begin;
    private Pattern end;
    private Pattern single;
    private Pattern javadoc;
    private Pattern groups;

    public DoccoCommentPatternsBuilder()
    {
    }

    public DoccoCommentPatternsBuilder multilineBegin(String pattern)
    {
        this.begin = Pattern.compile(pattern);
        return this;
    }

    public DoccoCommentPatternsBuilder multilineEnd(String pattern)
    {
        this.end = Pattern.compile(pattern);
        return this;
    }

    public DoccoCommentPatternsBuilder singleLine(String pattern)
    {
        this.single = Pattern.compile(pattern);
        return this;
    }

    public DoccoCommentPatternsBuilder javadoc(String pattern)
    {
        this.javadoc = Pattern.compile(pattern);
        return this;
    }

    public DoccoCommentPatternsBuilder groups(String pattern)
    {
        this.groups = Pattern.compile(pattern);
        return this;
    }

    public DoccoCommentPatterns build()
    {
        return new DoccoCommentPatternsImpl(begin,end,single,javadoc,groups);
    }

    public class DoccoCommentPatternsImpl implements DoccoCommentPatterns
    {
        private Pattern begin;
        private Pattern end;
        private Pattern single;
        private Pattern javadoc;
        private Pattern groups;

        private DoccoCommentPatternsImpl(Pattern begin, Pattern end, Pattern single, Pattern javadoc, Pattern groups)
        {
            this.begin = begin;
            this.end = end;
            this.single = single;
            this.javadoc = javadoc;
            this.groups = groups;

            Pattern neverMatches = Pattern.compile(DoccoCommentPatterns.NEVER_MATCHES);

            if(null == this.begin)
            {
                this.begin = neverMatches;
            }

            if(null == this.end)
            {
                this.end = neverMatches;
            }

            if(null == this.single)
            {
                this.single = neverMatches;
            }

            if(null == this.javadoc)
            {
                this.javadoc = neverMatches;
            }

            if(null == this.groups)
            {
                this.groups = neverMatches;
            }
        }

        @Override
        public Pattern getSingleLinePattern()
        {
            return single;
        }

        @Override
        public Pattern getMultilineBeginPattern()
        {
            return begin;
        }

        @Override
        public Pattern getMultilineEndPattern()
        {
            return end;
        }

        @Override
        public Pattern getJavadocBeginPattern()
        {
            return javadoc;
        }

        @Override
        public Pattern getGroupsPattern()
        {
            return groups;
        }
    }


}
