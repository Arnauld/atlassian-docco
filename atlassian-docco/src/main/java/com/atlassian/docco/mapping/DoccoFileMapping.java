package com.atlassian.docco.mapping;

/**
 * @since 1.0
 */
public class DoccoFileMapping
{
    private DoccoCommentPatterns patterns;
    private String syntaxName;
    private boolean forceKeepJavadoc;

    public DoccoFileMapping(DoccoCommentPatterns patterns, String syntaxName, boolean forceKeepJavadoc)
    {
        this.patterns = patterns;
        this.syntaxName = syntaxName;
        this.forceKeepJavadoc = forceKeepJavadoc;
    }

    public DoccoCommentPatterns getPatterns()
    {
        return patterns;
    }

    public String getSyntaxName()
    {
        return syntaxName;
    }

    public boolean forceKeepJavadoc()
    {
        return forceKeepJavadoc;
    }
}
