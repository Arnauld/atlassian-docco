package com.atlassian.docco.mapping;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;

import org.apache.commons.io.FilenameUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 1.0
 */
public class DoccoFileMappingManager
{
    private Map<String, DoccoFileMapping> patternsMap;

    public DoccoFileMappingManager()
    {
        this.patternsMap = new HashMap<String, DoccoFileMapping>();
        patternsMap.put("java", new DoccoFileMapping(DoccoCommentPatternsBuilder.CODE_PATTERNS,"java",false));
        patternsMap.put("scala", new DoccoFileMapping(DoccoCommentPatternsBuilder.CODE_PATTERNS,"java",false));
        patternsMap.put("js", new DoccoFileMapping(DoccoCommentPatternsBuilder.CODE_PATTERNS,"javascript",false));
        patternsMap.put("css", new DoccoFileMapping(DoccoCommentPatternsBuilder.CODE_PATTERNS,"css",false));

        patternsMap.put("html", new DoccoFileMapping(DoccoCommentPatternsBuilder.MARKUP_PATTERNS,"html",false));
        patternsMap.put("htm", new DoccoFileMapping(DoccoCommentPatternsBuilder.MARKUP_PATTERNS,"html",false));
        patternsMap.put("xhtml", new DoccoFileMapping(DoccoCommentPatternsBuilder.MARKUP_PATTERNS,"html",false));
        patternsMap.put("xml", new DoccoFileMapping(DoccoCommentPatternsBuilder.MARKUP_PATTERNS,"xml",false));
        patternsMap.put("ftl", new DoccoFileMapping(DoccoCommentPatternsBuilder.MARKUP_PATTERNS,"html",false));
        patternsMap.put("vm", new DoccoFileMapping(DoccoCommentPatternsBuilder.MARKUP_PATTERNS,"html",false));
        patternsMap.put("vtl", new DoccoFileMapping(DoccoCommentPatternsBuilder.MARKUP_PATTERNS,"html",false));
        patternsMap.put("soy", new DoccoFileMapping(DoccoCommentPatternsBuilder.CODE_PATTERNS,"xml",true));

        patternsMap.put("properties", new DoccoFileMapping(DoccoCommentPatternsBuilder.TEXT_PATTERNS,"bash",false));
    }

    public DoccoCommentPatterns getPatternsForFileExtension(String extension)
    {
        checkNotNull(extension);
        DoccoFileMapping doccoFileMapping = patternsMap.get(extension.toLowerCase());
        checkNotNull(doccoFileMapping, "Extension %s not supported", extension);
        return doccoFileMapping.getPatterns();
    }

    public DoccoCommentPatterns getPatternsForFile(File file)
    {
        checkNotNull(file);

        return getPatternsForFileExtension(FilenameUtils.getExtension(file.getName()));
    }

    public String getSyntaxForFileExtension(String extension)
    {
        checkNotNull(extension);
        return patternsMap.get(extension.toLowerCase()).getSyntaxName();
    }

    public String getSyntaxForFile(File file)
    {
        checkNotNull(file);

        return getSyntaxForFileExtension(FilenameUtils.getExtension(file.getName()));
    }
    
    public boolean getForceKeepJavadocForFile(File file)
    {
        checkNotNull(file);
        return getForceKeepJavadocForFileExtension(FilenameUtils.getExtension(file.getName()));
    }

    private boolean getForceKeepJavadocForFileExtension(String extension)
    {
        checkNotNull(extension);
        return patternsMap.get(extension.toLowerCase()).forceKeepJavadoc();
    }

    public void addMappingForFileExtension(String extension, DoccoFileMapping mapping)
    {
        checkNotNull(extension);
        checkNotNull(mapping);

        patternsMap.put(extension,mapping);
    }

    public void addMappingForFile(File file, DoccoFileMapping mapping)
    {
        checkNotNull(file);
        checkNotNull(mapping);

        addMappingForFileExtension(FilenameUtils.getExtension(file.getName()),mapping);
    }

    public List<String> getMappedFileExtensions()
    {
        return ImmutableList.copyOf(patternsMap.keySet());
    }
}
