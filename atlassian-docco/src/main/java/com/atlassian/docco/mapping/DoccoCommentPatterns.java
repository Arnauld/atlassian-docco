package com.atlassian.docco.mapping;

import java.util.regex.Pattern;

/**
 * @since 1.0
 */
public interface DoccoCommentPatterns
{
    public final static String NEVER_MATCHES = "(?!x)x";
    public final static String CODE_DOCCO_GROUPS = "^\\s*/\\*!!\\s*(.*?)\\*/";
    public final static String CODE_DOCCO_SINGLE_LINE = "^\\s*/\\*!\\s*(.*?)\\*/";
    public final static String CODE_DOCCO_MULTI_BEGIN = "^(\\s*)/\\*!\\s*(.*)";
    public final static String CODE_DOCCO_MULTI_END = "(.*?)\\*/\\s*";
    public final static String CODE_JAVADOC_BEGIN = "^(\\s*)/\\*\\*(.*)";

    public final static String MARKUP_DOCCO_GROUPS = "^\\s*<!--!!\\s*(.*?)-->";
    public final static String MARKUP_DOCCO_SINGLE_LINE = "^\\s*<!--!\\s*(.*?)-->";
    public final static String MARKUP_DOCCO_MULTI_BEGIN = "^(\\s*)<!--!\\s*(.*)";
    public final static String MARKUP_DOCCO_MULTI_END = "(.*?)-->\\s*";

    public final static String TEXT_DOCCO_GROUPS = "^\\s*\\#!![\\s]*(.*?)\\n";
    public final static String TEXT_DOCCO_SINGLE_LINE = "^\\s*\\#![\\s]*(.*?)\\n";

    Pattern getSingleLinePattern();
    Pattern getMultilineBeginPattern();
    Pattern getMultilineEndPattern();
    Pattern getJavadocBeginPattern();
    Pattern getGroupsPattern();
}
