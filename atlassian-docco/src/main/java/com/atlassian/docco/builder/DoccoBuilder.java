package com.atlassian.docco.builder;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.docco.Docco;
import com.atlassian.docco.mapping.DoccoFileMappingManager;

import org.pegdown.PegDownProcessor;

import static com.google.common.base.Preconditions.checkState;

/**
 * @since 1.0
 */
public class DoccoBuilder
{
    private URL hSoyTemplate;
    private URL vSoyTemplate;
    private Boolean stripJavadoc;
    private DoccoFileMappingManager fileMappings;
    private PegDownProcessor pegDown;
    private Boolean includeDefaultResources;
    private List<File> customResources;

    public DoccoBuilder()
    {
    }

    public DoccoBuilder horizontalTemplate(File soyTemplate)
    {
        try
        {
            this.hSoyTemplate = soyTemplate.toURI().toURL();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return this;
    }

    public DoccoBuilder horizontalTemplate(URL soyTemplate)
    {
            this.hSoyTemplate = soyTemplate;

        return this;
    }

    public DoccoBuilder verticalTemplate(File soyTemplate)
    {
        try
        {
            this.vSoyTemplate = soyTemplate.toURI().toURL();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return this;
    }

    public DoccoBuilder verticalTemplate(URL soyTemplate)
    {
        this.vSoyTemplate = soyTemplate;

        return this;
    }

    public DoccoBuilder stripJavadoc(Boolean stripJavadoc)
    {
        this.stripJavadoc = stripJavadoc;
        return this;
    }

    public DoccoBuilder fileMappings(DoccoFileMappingManager fileMappings)
    {
        this.fileMappings = fileMappings;
        return this;
    }

    public DoccoBuilder pegdownProcessor(PegDownProcessor pegDown)
    {
        this.pegDown = pegDown;
        return this;
    }

    public DoccoBuilder includeDefaultResources(Boolean defaultResources)
    {
        this.includeDefaultResources = defaultResources;
        return this;
    }

    public DoccoBuilder resources(List<File> customResources)
    {
        this.customResources = customResources;
        return this;
    }

    public Docco build()
    {
        if(null == hSoyTemplate)
        {
            try
            {
                hSoyTemplate = Docco.getDefaultHorizontalTemplate();
            }
            catch (URISyntaxException e)
            {
                throw new IllegalStateException("Unable to load default horizontal soy template!", e);
            }
        }

        if(null == vSoyTemplate)
        {
            try
            {
                vSoyTemplate = Docco.getDefaultVerticalTemplate();
            }
            catch (URISyntaxException e)
            {
                throw new IllegalStateException("Unable to load default vertical soy template!", e);
            }
        }

        if(null == stripJavadoc)
        {
            this.stripJavadoc = Boolean.TRUE;
        }

        if(null == fileMappings)
        {
            this.fileMappings = new DoccoFileMappingManager();
        }

        if(null == pegDown)
        {
            this.pegDown = new PegDownProcessor();
        }

        if(null == includeDefaultResources)
        {
            this.includeDefaultResources = Boolean.TRUE;
        }

        if(null == customResources)
        {
            customResources = new ArrayList<File>();
        }

        return new Docco(hSoyTemplate,vSoyTemplate,fileMappings,stripJavadoc.booleanValue(),includeDefaultResources.booleanValue(),customResources,pegDown);
    }
}

