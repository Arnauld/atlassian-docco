package com.atlassian.docco;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;

/**
 * @since 1.0
 */
public final class Section
{
    private String doc;
    private String code;
    private String markdown;
    private boolean committed;
    private boolean hidden;
    private String title;
    private PegDownProcessor pegDown;

    public Section()
    {
        this(new PegDownProcessor());
    }

    public Section(PegDownProcessor pegDown)
    {
        this.doc = "";
        this.code = "";
        this.markdown = null;
        this.committed = false;
        this.hidden = false;
        this.title = "";
        this.pegDown = pegDown;
    }

    public Section addDoc(String doc)
    {
        this.doc += doc + "\n";
        this.markdown = null;

        return this;
    }

    public Section addCode(String code)
    {
        this.code += code + "\n";
        this.committed = true;

        return this;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public boolean isHidden()
    {
        return hidden;
    }

    public void setHidden(boolean hidden)
    {
        this.hidden = hidden;
    }

    public boolean isEmpty()
    {
        return (StringUtils.isBlank(doc) && StringUtils.isBlank(code));
    }

    public String getCode()
    {
        return trimNewLines(code);
    }

    public String getDoc()
    {
        if(null == markdown)
        {
            markdown = pegDown.markdownToHtml(doc);
        }

        return markdown;
    }

    public String getDocWithTokenReplacement(Map<String,String> replaceMap)
    {
        String replacedDoc = doc;
        for(Map.Entry<String,String> entry : replaceMap.entrySet())
        {
            replacedDoc = StringUtils.replace(doc,entry.getKey(),entry.getValue());
        }

        if(null == markdown)
        {
            markdown = pegDown.markdownToHtml(replacedDoc);
        }

        return markdown;
    }

    public boolean isCommitted()
    {
        return committed;
    }

    private String trimNewLines(String s)
    {
        return s.replaceAll("^\\n+(.*)", "$1").replaceAll("(.*?)\\n+$", "$1");
    }


}
