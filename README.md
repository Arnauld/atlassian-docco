### Project Information
Copyright (c) 2012 Atlassian Software Systems Pty Ltd  
under the Apache 2 License: <http://www.apache.org/licenses/LICENSE-2.0>

Source Repository: <https://bitbucket.org/doklovic_atlassian/atlassian-docco>

Issue Tracking: <https://bitbucket.org/doklovic_atlassian/atlassian-docco/issues>

Wiki: <https://bitbucket.org/doklovic_atlassian/atlassian-docco/wiki>

## What Is Docco?
Docco offers an alternative approach to maintain source code documentation in a literate programming style.

Unlike standard documentation tools, Docco encourages developers to express their thoughts consistently by providing explanations, instructions, HOWTOs and usage scenarios side-by-side with corresponding code fragments.  
It produces HTML that displays your comments alongside your code.

The [Docco Project](http://jashkenas.github.com/docco) was originally a "quick and dirty" coffee script project used to document coffee scripts.  
Later [Circumflex](http://circumflex.ru/projects/docco/index.html) ported Docco to scala for use in scala projects.

And now there's:

### Atlassian Docco

A port of the Docco project to JAVA borrowing ideas from the Circumflex scala port.

This implementation is written in JAVA, but can be used to docco just about any file format that supports comments.

To process the markdown in docco comments, we use the excellent [Pegdown Project](https://github.com/sirthias/pegdown)  
And to provide syntax highlighting we use [Highlight JS](http://softwaremaniacs.org/soft/highlight/en/)

Along with the core library, Atlassian Docco also provides a maven plugin to easily generate docco for a maven project.

_A command-line interface is coming soon for use with non-maven projects._

### Features
* Easily format docco comments using [Markdown Syntax](http://daringfireball.net/projects/markdown/)
* Ability to add "header" comments above the code view
* Builds both horizontal and vertical displays
* Customizable navigation
* User switchable syntax highlighting themes
* User switchable layouts (horizontal or vertical)
* Auto and User based code-folding
* Numbered permalinks to each section in a file
* Out of the box support for:
    * java
    * scala
    * js (javascript)
    * css
    * html
    * xml
    * ftl (freemarker)
    * vm (velocity)
    * soy (google closure)
* Ability to map custom file types
* Ability to use your own soy templates to customize output
* Ability to provide custom resources (css, js, images, etc)

### Using Atlassian Docco
For information about writing docco comments, see: [Writing Docco Comments](https://bitbucket.org/doklovic_atlassian/atlassian-docco/wiki/writing-docco-comments)

For Maven usage see: [Running Docco With Maven](https://bitbucket.org/doklovic_atlassian/atlassian-docco/wiki/docco-maven)

For using docco in JAVA see: [Running Docco From Java](https://bitbucket.org/doklovic_atlassian/atlassian-docco/wiki/docco-java)

For customizing output see: [Using Custom Templates](https://bitbucket.org/doklovic_atlassian/atlassian-docco/wiki/docco-custom-templates)

